-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 26, 2019 at 04:13 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rpl_lettering`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `password`, `fullname`, `phone`, `email`, `created_at`, `updated_at`) VALUES
(1, 'admin1', 'admin', 'Administrator', '0', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `dispositions`
--

CREATE TABLE `dispositions` (
  `id` bigint(255) NOT NULL,
  `letter_id` bigint(255) NOT NULL,
  `disposition_date` datetime NOT NULL,
  `due_date` datetime DEFAULT NULL,
  `importance_level` int(10) DEFAULT NULL,
  `advancement` longtext,
  `message` varchar(250) DEFAULT NULL,
  `scan` varchar(250) DEFAULT NULL,
  `acknowledge` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dispositions`
--

INSERT INTO `dispositions` (`id`, `letter_id`, `disposition_date`, `due_date`, `importance_level`, `advancement`, `message`, `scan`, `acknowledge`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-04-01 14:16:31', '2019-04-01 14:16:31', 5, '[{\"id\":4,\"advancement\":\"Wakili/Hadiri/Terima/Laporkan Hasilnya\",\"type\":\"check\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:08:27.000Z\",\"updated_at\":\"2019-04-01T07:08:27.000Z\",\"selected\":true},{\"id\":5,\"advancement\":\"Agendakan/Persiapkan/Koordinasikan\",\"type\":\"check\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:08:42.000Z\",\"updated_at\":\"2019-04-01T07:08:42.000Z\",\"selected\":true},{\"id\":6,\"advancement\":\"Tugaskan Kasubbag. TU/Waka/Kakomp/Guru/Staf\",\"type\":\"check\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:09:01.000Z\",\"updated_at\":\"2019-04-01T07:09:01.000Z\",\"selected\":true},{\"id\":7,\"advancement\":\"Selesaikan sesuai ketentuan/peraturan yang berlaku\",\"type\":\"check\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:09:12.000Z\",\"updated_at\":\"2019-04-01T07:09:12.000Z\",\"selected\":true},{\"id\":8,\"advancement\":\"Pelajari/Telaah sarannya\",\"type\":\"check\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:09:26.000Z\",\"updated_at\":\"2019-04-01T07:09:26.000Z\",\"selected\":true},{\"id\":9,\"advancement\":\"Untuk dijawab/dicatat/FILE/Pedoman\",\"type\":\"check\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:09:48.000Z\",\"updated_at\":\"2019-04-01T07:09:48.000Z\",\"selected\":true},{\"id\":10,\"advancement\":\"Untuk ditindaklanjuti/difasilitasi/dipenuhi sesuai ketentuan\",\"type\":\"check\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:10:07.000Z\",\"updated_at\":\"2019-04-01T07:10:07.000Z\",\"selected\":true},{\"id\":11,\"advancement\":\"Untuk dibantu/diketahui/dipantau perkembangannya\",\"type\":\"check\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:10:19.000Z\",\"updated_at\":\"2019-04-01T07:10:19.000Z\",\"selected\":true},{\"id\":12,\"advancement\":\"Siapkan pointer/Sambutan/Bahan\",\"type\":\"check\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:10:29.000Z\",\"updated_at\":\"2019-04-01T07:10:29.000Z\",\"selected\":true},{\"id\":13,\"advancement\":\"Untuk bahan rapat/bahan lebih lanjut\",\"type\":\"check\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:10:39.000Z\",\"updated_at\":\"2019-04-01T07:10:39.000Z\",\"selected\":true},{\"id\":14,\"advancement\":\"ACC, sesuai dengan ketentuan yang berlaku\",\"type\":\"check\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:10:50.000Z\",\"updated_at\":\"2019-04-01T07:10:50.000Z\",\"selected\":true},{\"id\":15,\"advancement\":\"ACC, sesuai saran Saudara\",\"type\":\"check\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:11:00.000Z\",\"updated_at\":\"2019-04-01T07:11:00.000Z\",\"selected\":true},{\"id\":16,\"advancement\":\"Legalitas permohonan agar dikonfirmasikan ke\",\"type\":\"lanjut\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:11:21.000Z\",\"updated_at\":\"2019-04-01T07:15:56.000Z\",\"selected\":true}]', '[{\"id\":4,\"advancement\":\"Wakili/Hadiri/Terima/Laporkan Hasilnya\",\"type\":\"check\",\"options\":[],\"created_at\":\"2019-04-01T07:08:27.000Z\",\"updated_at\":\"2019-04-01T07:08:27.000Z\",\"selected\":true,\"active\":true},{\"id\":5,\"advancement\":\"Agendakan/Persiapkan/Koo', '1554190892.pdf', '2019-04-02 14:41:32', '2019-04-01 14:16:31', '2019-04-02 14:41:32'),
(2, 7, '2019-04-02 14:40:11', '2019-04-02 14:40:11', 13, '[{\"id\":7,\"advancement\":\"Selesaikan sesuai ketentuan/peraturan yang berlaku\",\"type\":\"check\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:09:12.000Z\",\"updated_at\":\"2019-04-01T07:09:12.000Z\",\"selected\":true},{\"id\":10,\"advancement\":\"Untuk ditindaklanjuti/difasilitasi/dipenuhi sesuai ketentuan\",\"type\":\"check\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:10:07.000Z\",\"updated_at\":\"2019-04-01T07:10:07.000Z\",\"selected\":true},{\"id\":11,\"advancement\":\"Untuk dibantu/diketahui/dipantau perkembangannya\",\"type\":\"check\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:10:19.000Z\",\"updated_at\":\"2019-04-01T07:10:19.000Z\",\"selected\":true},{\"id\":12,\"advancement\":\"Siapkan pointer/Sambutan/Bahan\",\"type\":\"check\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:10:29.000Z\",\"updated_at\":\"2019-04-01T07:10:29.000Z\",\"selected\":true},{\"id\":13,\"advancement\":\"Untuk bahan rapat/bahan lebih lanjut\",\"type\":\"check\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:10:39.000Z\",\"updated_at\":\"2019-04-01T07:10:39.000Z\",\"selected\":true},{\"id\":14,\"advancement\":\"ACC, sesuai dengan ketentuan yang berlaku\",\"type\":\"check\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:10:50.000Z\",\"updated_at\":\"2019-04-01T07:10:50.000Z\",\"selected\":true},{\"id\":15,\"advancement\":\"ACC, sesuai saran Saudara\",\"type\":\"check\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:11:00.000Z\",\"updated_at\":\"2019-04-01T07:11:00.000Z\",\"selected\":true},{\"id\":16,\"advancement\":\"Legalitas permohonan agar dikonfirmasikan ke\",\"type\":\"lanjut\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:11:21.000Z\",\"updated_at\":\"2019-04-01T07:15:56.000Z\",\"selected\":true}]', '[{\"id\":7,\"advancement\":\"Selesaikan sesuai ketentuan/peraturan yang berlaku\",\"type\":\"check\",\"options\":[],\"created_at\":\"2019-04-01T07:09:12.000Z\",\"updated_at\":\"2019-04-01T07:09:12.000Z\",\"selected\":true,\"active\":true},{\"id\":10,\"advancement\":\"Untuk ditin', '1554276577.pdf', '2019-04-03 14:29:37', '2019-04-02 14:40:11', '2019-04-03 14:29:37'),
(3, 9, '2019-04-29 08:47:09', '2019-04-29 08:47:09', 2, '[{\"id\":4,\"advancement\":\"Wakili/Hadiri/Terima/Laporkan Hasilnya\",\"type\":\"check\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:08:27.000Z\",\"updated_at\":\"2019-04-01T07:08:27.000Z\",\"selected\":true},{\"id\":5,\"advancement\":\"Agendakan/Persiapkan/Koordinasikan\",\"type\":\"check\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:08:42.000Z\",\"updated_at\":\"2019-04-01T07:08:42.000Z\",\"selected\":true},{\"id\":6,\"advancement\":\"Tugaskan Kasubbag. TU/Waka/Kakomp/Guru/Staf\",\"type\":\"check\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:09:01.000Z\",\"updated_at\":\"2019-04-01T07:09:01.000Z\",\"selected\":true},{\"id\":7,\"advancement\":\"Selesaikan sesuai ketentuan/peraturan yang berlaku\",\"type\":\"check\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:09:12.000Z\",\"updated_at\":\"2019-04-01T07:09:12.000Z\",\"selected\":true},{\"id\":8,\"advancement\":\"Pelajari/Telaah sarannya\",\"type\":\"check\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:09:26.000Z\",\"updated_at\":\"2019-04-01T07:09:26.000Z\",\"selected\":true},{\"id\":9,\"advancement\":\"Untuk dijawab/dicatat/FILE/Pedoman\",\"type\":\"check\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:09:48.000Z\",\"updated_at\":\"2019-04-01T07:09:48.000Z\",\"selected\":true},{\"id\":10,\"advancement\":\"Untuk ditindaklanjuti/difasilitasi/dipenuhi sesuai ketentuan\",\"type\":\"check\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:10:07.000Z\",\"updated_at\":\"2019-04-01T07:10:07.000Z\",\"selected\":true},{\"id\":11,\"advancement\":\"Untuk dibantu/diketahui/dipantau perkembangannya\",\"type\":\"check\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:10:19.000Z\",\"updated_at\":\"2019-04-01T07:10:19.000Z\",\"selected\":true},{\"id\":12,\"advancement\":\"Siapkan pointer/Sambutan/Bahan\",\"type\":\"check\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:10:29.000Z\",\"updated_at\":\"2019-04-01T07:10:29.000Z\",\"selected\":true},{\"id\":13,\"advancement\":\"Untuk bahan rapat/bahan lebih lanjut\",\"type\":\"check\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:10:39.000Z\",\"updated_at\":\"2019-04-01T07:10:39.000Z\",\"selected\":true},{\"id\":14,\"advancement\":\"ACC, sesuai dengan ketentuan yang berlaku\",\"type\":\"check\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:10:50.000Z\",\"updated_at\":\"2019-04-01T07:10:50.000Z\",\"selected\":true},{\"id\":15,\"advancement\":\"ACC, sesuai saran Saudara\",\"type\":\"check\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:11:00.000Z\",\"updated_at\":\"2019-04-01T07:11:00.000Z\",\"selected\":true},{\"id\":16,\"advancement\":\"Legalitas permohonan agar dikonfirmasikan ke\",\"type\":\"lanjut\",\"options\":[{\"value\":\"\"}],\"created_at\":\"2019-04-01T07:11:21.000Z\",\"updated_at\":\"2019-04-01T07:15:56.000Z\",\"selected\":true}]', NULL, NULL, NULL, '2019-04-29 08:47:09', '2019-04-29 08:47:09');

-- --------------------------------------------------------

--
-- Table structure for table `disposition_advancement_master`
--

CREATE TABLE `disposition_advancement_master` (
  `id` int(11) NOT NULL,
  `advancement` varchar(150) NOT NULL,
  `type` varchar(100) NOT NULL,
  `options` varchar(250) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disposition_advancement_master`
--

INSERT INTO `disposition_advancement_master` (`id`, `advancement`, `type`, `options`, `created_at`, `updated_at`) VALUES
(4, 'Wakili/Hadiri/Terima/Laporkan Hasilnya', 'check', '[{\"value\":\"\"}]', '2019-04-01 14:08:27', '2019-04-01 14:08:27'),
(5, 'Agendakan/Persiapkan/Koordinasikan', 'check', '[{\"value\":\"\"}]', '2019-04-01 14:08:42', '2019-04-01 14:08:42'),
(6, 'Tugaskan Kasubbag. TU/Waka/Kakomp/Guru/Staf', 'check', '[{\"value\":\"\"}]', '2019-04-01 14:09:01', '2019-04-01 14:09:01'),
(7, 'Selesaikan sesuai ketentuan/peraturan yang berlaku', 'check', '[{\"value\":\"\"}]', '2019-04-01 14:09:12', '2019-04-01 14:09:12'),
(8, 'Pelajari/Telaah sarannya', 'check', '[{\"value\":\"\"}]', '2019-04-01 14:09:26', '2019-04-01 14:09:26'),
(9, 'Untuk dijawab/dicatat/FILE/Pedoman', 'check', '[{\"value\":\"\"}]', '2019-04-01 14:09:48', '2019-04-01 14:09:48'),
(10, 'Untuk ditindaklanjuti/difasilitasi/dipenuhi sesuai ketentuan', 'check', '[{\"value\":\"\"}]', '2019-04-01 14:10:07', '2019-04-01 14:10:07'),
(11, 'Untuk dibantu/diketahui/dipantau perkembangannya', 'check', '[{\"value\":\"\"}]', '2019-04-01 14:10:19', '2019-04-01 14:10:19'),
(12, 'Siapkan pointer/Sambutan/Bahan', 'check', '[{\"value\":\"\"}]', '2019-04-01 14:10:29', '2019-04-01 14:10:29'),
(13, 'Untuk bahan rapat/bahan lebih lanjut', 'check', '[{\"value\":\"\"}]', '2019-04-01 14:10:39', '2019-04-01 14:10:39'),
(14, 'ACC, sesuai dengan ketentuan yang berlaku', 'check', '[{\"value\":\"\"}]', '2019-04-01 14:10:50', '2019-04-01 14:10:50'),
(15, 'ACC, sesuai saran Saudara', 'check', '[{\"value\":\"\"}]', '2019-04-01 14:11:00', '2019-04-01 14:11:00'),
(16, 'Legalitas permohonan agar dikonfirmasikan ke', 'lanjut', '[{\"value\":\"\"}]', '2019-04-01 14:11:21', '2019-04-01 14:15:56');

-- --------------------------------------------------------

--
-- Table structure for table `disposition_targets`
--

CREATE TABLE `disposition_targets` (
  `id` bigint(255) NOT NULL,
  `disposition_id` bigint(255) NOT NULL,
  `disposition_order` int(11) NOT NULL,
  `target_name` varchar(255) NOT NULL,
  `target_id` varchar(250) NOT NULL,
  `status` varchar(50) NOT NULL,
  `message` varchar(150) DEFAULT NULL,
  `command` varchar(150) DEFAULT NULL,
  `aswad_enrollment_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disposition_targets`
--

INSERT INTO `disposition_targets` (`id`, `disposition_id`, `disposition_order`, `target_name`, `target_id`, `status`, `message`, `command`, `aswad_enrollment_id`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 'Hikmat Daviyana, S.Pd|Kaprog RPL', 'NIP 19760200192982381', 'ok', NULL, NULL, NULL, '2019-04-01 14:16:31', '2019-04-01 14:16:31'),
(2, 2, 0, 'Drs. Firdaus, M.Pd|Kepala Sekolah', 'NIP 765856', 'ok', NULL, NULL, NULL, '2019-04-02 14:40:11', '2019-04-02 14:40:11'),
(3, 2, 1, 'Hikmat Daviyana, S.Pd|Kaprog RPL', 'NIP 19760200192982381', 'ok', NULL, NULL, NULL, '2019-04-02 14:40:11', '2019-04-02 14:40:11'),
(4, 3, 0, 'Drs. Firdaus, M.Pd|Kepala Sekolah', 'NIP 765856', 'ok', NULL, NULL, NULL, '2019-04-29 08:47:09', '2019-04-29 08:47:09');

-- --------------------------------------------------------

--
-- Table structure for table `disposition_target_master`
--

CREATE TABLE `disposition_target_master` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `id_type` varchar(20) NOT NULL,
  `id_number` varchar(50) NOT NULL,
  `active` int(2) NOT NULL,
  `role` varchar(75) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disposition_target_master`
--

INSERT INTO `disposition_target_master` (`id`, `name`, `id_type`, `id_number`, `active`, `role`, `created_at`, `updated_at`) VALUES
(1, 'Hikmat Daviyana, S.Pd', 'NIP', '19760200192982381', 1, 'Kaprog RPL', '2019-03-22 16:50:18', '2019-03-22 16:57:54'),
(2, 'Alfi Rahman Hakim, S.Kom', 'NUPTK', '', 1, 'Guru RPL', '2019-03-26 10:18:31', '2019-03-26 13:24:30'),
(3, 'Drs. Firdaus, M.Pd', 'NIP', '765856', 1, 'Kepala Sekolah', '2019-04-02 14:39:24', '2019-04-02 14:39:24');

-- --------------------------------------------------------

--
-- Table structure for table `letters`
--

CREATE TABLE `letters` (
  `id` bigint(255) NOT NULL,
  `periode` char(6) NOT NULL,
  `letter_date` datetime NOT NULL,
  `letter_no` varchar(150) NOT NULL,
  `title` varchar(150) NOT NULL,
  `registry_no` int(11) NOT NULL,
  `sender` varchar(200) NOT NULL,
  `brief_content` varchar(250) NOT NULL,
  `scan` varchar(250) NOT NULL,
  `status` varchar(150) NOT NULL,
  `direction` varchar(5) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `origin` bigint(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `letters`
--

INSERT INTO `letters` (`id`, `periode`, `letter_date`, `letter_no`, `title`, `registry_no`, `sender`, `brief_content`, `scan`, `status`, `direction`, `admin_id`, `created_at`, `updated_at`, `origin`) VALUES
(1, '201903', '2019-03-26 11:33:00', 'A/001/Disdik/2019', 'Undangan Seminar Student Day', 1, 'Dinas Pendidikan', 'Undangan Seminar Student Day Kabupaten Bogor untuk Guru Produktif', '1553574880.pdf', '', 'in', 1, '2019-03-26 11:34:40', '2019-03-26 11:34:40', NULL),
(6, '201903', '2019-03-28 10:48:45', 'A/001/Disdik/2019sdaw', 'wdawd', 2, 'SMK Negeri 1 Cibinong', 'awdawd', '1553744955.pdf', '', 'out', 1, '2019-03-28 10:49:15', '2019-03-28 10:49:15', 1),
(7, '201904', '2019-04-01 14:35:00', '001/apa/baa', 'Rekrutmen', 1, 'PT Anugrah', 'janwdawndij', '1554190732.pdf', '', 'in', 1, '2019-04-02 14:38:52', '2019-04-02 14:38:52', NULL),
(8, '201904', '2019-04-01 14:42:00', 'BALAS/A/001/Disdik/2019', 'balasan', 2, 'SMK Negeri 1 Cibinong', 'iyghuy', '1554191014.pdf', '', 'out', 1, '2019-04-02 14:43:34', '2019-04-02 14:43:34', 1),
(9, '201904', '2019-04-29 08:46:20', 'ab/004/jkt', '-', 3, 'Indonesia', '', '1556502407.pdf', '', 'in', 1, '2019-04-29 08:46:47', '2019-04-29 08:46:47', NULL),
(10, '201905', '2019-05-04 14:30:25', 'AB001', '-', 1, 'SMKN1CIbinong', '-', '1556955049.pdf', '', 'in', 1, '2019-05-04 14:30:48', '2019-05-04 14:30:49', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dispositions`
--
ALTER TABLE `dispositions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `disposition_advancement_master`
--
ALTER TABLE `disposition_advancement_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `disposition_targets`
--
ALTER TABLE `disposition_targets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `disposition_target_master`
--
ALTER TABLE `disposition_target_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `letters`
--
ALTER TABLE `letters`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `dispositions`
--
ALTER TABLE `dispositions`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `disposition_advancement_master`
--
ALTER TABLE `disposition_advancement_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `disposition_targets`
--
ALTER TABLE `disposition_targets`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `disposition_target_master`
--
ALTER TABLE `disposition_target_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `letters`
--
ALTER TABLE `letters`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
