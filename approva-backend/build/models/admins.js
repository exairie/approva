"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _db = require("../db");

var _db2 = _interopRequireDefault(_db);

var _sequelize = require("sequelize");

var _sequelize2 = _interopRequireDefault(_sequelize);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _db2.default.define("admins", {
  id: {
    type: _sequelize2.default.INTEGER(11),
    allowNull: false,
    primaryKey: true,
    autoIncrement: true
  },
  username: {
    type: _sequelize2.default.STRING(50),
    allowNull: false
  },
  password: {
    type: _sequelize2.default.STRING(50),
    allowNull: false
  },
  fullname: {
    type: _sequelize2.default.STRING(50),
    allowNull: false
  },
  phone: {
    type: _sequelize2.default.STRING(50),
    allowNull: false
  },
  email: {
    type: _sequelize2.default.STRING(50),
    allowNull: false
  },
  created_at: {
    type: _sequelize2.default.DATE,
    allowNull: false
  },
  updated_at: {
    type: _sequelize2.default.DATE,
    allowNull: false
  }
}, {
  tableName: "admins"
});