"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _db = require("../db");

var _db2 = _interopRequireDefault(_db);

var _sequelize = require("sequelize");

var _sequelize2 = _interopRequireDefault(_sequelize);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* jshint indent: 2 */
exports.default = _db2.default.define("disposition_targets", {
  id: {
    type: _sequelize2.default.BIGINT,
    allowNull: false,
    primaryKey: true,
    autoIncrement: true
  },
  disposition_id: {
    type: _sequelize2.default.BIGINT,
    allowNull: false
  },
  target_name: {
    type: _sequelize2.default.STRING(255),
    allowNull: false
  },
  target_id: {
    type: _sequelize2.default.STRING(250),
    allowNull: false
  },
  status: {
    type: _sequelize2.default.STRING(50),
    allowNull: false
  },
  message: {
    type: _sequelize2.default.STRING(150),
    allowNull: true
  },
  command: {
    type: _sequelize2.default.STRING(150),
    allowNull: true
  },
  aswad_enrollment_id: {
    type: _sequelize2.default.INTEGER(11),
    allowNull: true
  },
  created_at: {
    type: _sequelize2.default.DATE,
    allowNull: false
  },
  updated_at: {
    type: _sequelize2.default.DATE,
    allowNull: false
  }
}, {
  tableName: "disposition_targets"
});