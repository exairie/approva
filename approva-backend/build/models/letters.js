"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _db = require("../db");

var _db2 = _interopRequireDefault(_db);

var _sequelize = require("sequelize");

var _sequelize2 = _interopRequireDefault(_sequelize);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* jshint indent: 2 */
exports.default = _db2.default.define("letters", {
  id: {
    type: _sequelize2.default.BIGINT,
    allowNull: false,
    primaryKey: true,
    autoIncrement: true
  },
  periode: {
    type: _sequelize2.default.CHAR(6),
    allowNull: false
  },
  letter_date: {
    type: _sequelize2.default.DATE,
    allowNull: false
  },
  letter_no: {
    type: _sequelize2.default.STRING(150),
    allowNull: false
  },
  registry_no: {
    type: _sequelize2.default.INTEGER(11),
    allowNull: false
  },
  sender: {
    type: _sequelize2.default.STRING(200),
    allowNull: false
  },
  brief_content: {
    type: _sequelize2.default.STRING(250),
    allowNull: false
  },
  scan: {
    type: _sequelize2.default.STRING(250),
    allowNull: false
  },
  status: {
    type: _sequelize2.default.STRING(150),
    allowNull: false
  },
  direction: {
    type: _sequelize2.default.STRING(5),
    allowNull: false
  },
  created_at: {
    type: _sequelize2.default.DATE,
    allowNull: false
  },
  updated_at: {
    type: _sequelize2.default.DATE,
    allowNull: false
  },
  origin: {
    type: _sequelize2.default.BIGINT,
    allowNull: true
  },
  admin_id: { type: _sequelize2.default.INTEGER, allowNull: false }
}, {
  tableName: "letters"
});