"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _db = require("../db");

var _db2 = _interopRequireDefault(_db);

var _sequelize = require("sequelize");

var _sequelize2 = _interopRequireDefault(_sequelize);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* jshint indent: 2 */
exports.default = _db2.default.define("dispositions", {
  id: {
    type: _sequelize2.default.BIGINT,
    allowNull: false,
    primaryKey: true,
    autoIncrement: true
  },
  letter_id: {
    type: _sequelize2.default.BIGINT,
    allowNull: false
  },
  disposition_date: {
    type: _sequelize2.default.DATE,
    allowNull: false
  },
  due_date: {
    type: _sequelize2.default.DATE,
    allowNull: false
  },
  importance_level: {
    type: _sequelize2.default.INTEGER(10),
    allowNull: true
  },
  advancement: {
    type: _sequelize2.default.STRING(250),
    allowNull: true
  },
  message: {
    type: _sequelize2.default.STRING(250),
    allowNull: true
  },
  scan: {
    type: _sequelize2.default.STRING(250),
    allowNull: true
  },
  created_at: {
    type: _sequelize2.default.DATE,
    allowNull: false
  },
  updated_at: {
    type: _sequelize2.default.DATE,
    allowNull: false
  }
}, {
  tableName: "dispositions"
});