"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Letter = exports.Disposition = exports.DispositionTarget = exports.Admin = undefined;

var _admins = require("./admins");

var _admins2 = _interopRequireDefault(_admins);

var _disposition_targets = require("./disposition_targets");

var _disposition_targets2 = _interopRequireDefault(_disposition_targets);

var _dispositions = require("./dispositions");

var _dispositions2 = _interopRequireDefault(_dispositions);

var _letters = require("./letters");

var _letters2 = _interopRequireDefault(_letters);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_dispositions2.default.hasMany(_disposition_targets2.default);
_disposition_targets2.default.belongsTo(_dispositions2.default);

_letters2.default.hasMany(_letters2.default, {
  as: "advancement",
  foreignKey: "origin",
  sourceKey: "id"
});
_letters2.default.belongsTo(_letters2.default, {
  as: "letter_origin",
  foreignKey: "origin",
  targetKey: "id"
});
_letters2.default.hasMany(_dispositions2.default);
_letters2.default.belongsTo(_admins2.default);
_admins2.default.hasMany(_letters2.default);
exports.Admin = _admins2.default;
exports.DispositionTarget = _disposition_targets2.default;
exports.Disposition = _dispositions2.default;
exports.Letter = _letters2.default;