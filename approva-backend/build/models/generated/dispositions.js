'use strict';

/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('dispositions', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    letter_id: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    disposition_date: {
      type: DataTypes.DATE,
      allowNull: false
    },
    due_date: {
      type: DataTypes.DATE,
      allowNull: false
    },
    importance_level: {
      type: DataTypes.INTEGER(10),
      allowNull: false
    },
    advancement: {
      type: DataTypes.STRING(250),
      allowNull: false
    },
    message: {
      type: DataTypes.STRING(250),
      allowNull: false
    },
    scan: {
      type: DataTypes.STRING(250),
      allowNull: true
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    tableName: 'dispositions'
  });
};