"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _sequelize = require("sequelize");

var _sequelize2 = _interopRequireDefault(_sequelize);

var _configs = require("./configs");

var cfg = _interopRequireWildcard(_configs);

var _momentTimezone = require("moment-timezone");

var _momentTimezone2 = _interopRequireDefault(_momentTimezone);

var _child_process = require("child_process");

var _child_process2 = _interopRequireDefault(_child_process);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const db = new _sequelize2.default(cfg.database, cfg.username, cfg.password, {
  dialect: "mysql",
  username: cfg.username,
  password: cfg.password,
  host: cfg.host,
  dialectOptions: {
    // useUTC:false
    typeCast: function (field, next) {
      // for reading from database
      if (field.type === "DATETIME") {
        var time = _momentTimezone2.default.tz(field.string(), "Asia/Jakarta");
        // console.log(time.toString());
        // console.log(time);
        return time;
      }
      return next();
    }
  },
  define: {
    underscored: true
  },
  timezone: "+07:00" //for writing to database,
});

let authenticate = () => {
  db.authenticate().then(() => {
    console.log("Database connected");
  }).catch(e => {
    console.log("Database connect failed. Retrying in 2 secs");
    console.log(e);
    setTimeout(() => {
      authenticate();
    }, 2000);
  });
};

authenticate();

exports.default = db;