"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.middleware = middleware;
exports.verify = verify;
exports.createJWToken = createJWToken;

var _jsonwebtoken = require("jsonwebtoken");

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _configs = require("../configs");

var _lodash = require("lodash");

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

async function middleware(req, res, next) {
  if (req.headers.token) {
    let token = req.headers.token;
    try {
      let tokenData = await verify(token);
      req.userdata = tokenData.data;

      next();
    } catch (e) {
      res.status(400).json(e);
    }
  } else {
    console.log("FORBIDDEN!");
    res.status(403).end();
  }
}
function verify(token) {
  return new Promise((res, rej) => {
    _jsonwebtoken2.default.verify(token, _configs.privatekey, (err, decodetoken) => {
      if (err || !decodetoken) {
        rej(err);
      }

      res(decodetoken);
    });
  });
}

function createJWToken(details) {
  if (!details) return "";
  console.log("Creating token");
  console.log(typeof details);

  if (typeof details !== "object") {
    details = {};
    return "";
  }

  console.log(details);

  if (!details.maxAge || typeof details.maxAge !== "number") {
    details.maxAge = 999999999;
  }

  console.log(details);

  details.sessionData = _lodash2.default.reduce(details.sessionData || {}, (memo, val, key) => {
    if (typeof val !== "function" && key !== "password") {
      memo[key] = val;
    }
    return memo;
  }, {});

  console.log(details);

  let token = _jsonwebtoken2.default.sign({
    data: details
  }, _configs.privatekey, {
    expiresIn: details.maxAge,
    algorithm: "HS256"
  });

  return token;
}