"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Letters = require("./routes/Letters");

var _Letters2 = _interopRequireDefault(_Letters);

var _Disposition = require("./routes/Disposition");

var _Disposition2 = _interopRequireDefault(_Disposition);

var _Administrator = require("./routes/Administrator");

var _Administrator2 = _interopRequireDefault(_Administrator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = app => {
  app.use("/letters", _Letters2.default);
  app.use("/dispositions", _Disposition2.default);
  app.use("/administrator", _Administrator2.default);
};