"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DispositionStorage = exports.DocumentStorage = undefined;

var _multer = require("multer");

var _multer2 = _interopRequireDefault(_multer);

var _moment = require("moment");

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const DocumentUploadStorage = _multer2.default.diskStorage({
  destination: "./uploads/documents",
  filename: (req, file, callback) => {
    callback(null, (0, _moment2.default)().unix().toString());
  }
});
const DispositionUploadStorage = _multer2.default.diskStorage({
  destination: "./uploads/dispositions",
  filename: (req, file, callback) => {
    callback(null, (0, _moment2.default)().unix().toString());
  }
});

const DocumentStorage = exports.DocumentStorage = (0, _multer2.default)({ storage: DocumentUploadStorage });
const DispositionStorage = exports.DispositionStorage = (0, _multer2.default)({ storage: DispositionUploadStorage });