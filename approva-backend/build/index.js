"use strict";

var _express = require("express");

var _express2 = _interopRequireDefault(_express);

var _cors = require("cors");

var _cors2 = _interopRequireDefault(_cors);

var _bodyParser = require("body-parser");

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _router = require("./router");

var _router2 = _interopRequireDefault(_router);

var _http = require("http");

var _http2 = _interopRequireDefault(_http);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var server = (0, _express2.default)();
var http = _http2.default.Server(server);

server.use((req, res, next) => {
  console.log(`Incoming request ${req.method} : ${req.path}`);
  next();
});

server.use((0, _cors2.default)());
server.use(_bodyParser2.default.urlencoded());
server.use(_bodyParser2.default.json());

server.use("/docscan", _express2.default.static("./uploads/documents"));

(0, _router2.default)(server);

server.get("/ba", (req, res) => {
  console.log("SENDING ");
  res.send("Hello World").end();
});

server.listen(process.env.PORT || 4480, () => {
  console.log("Server started");
});