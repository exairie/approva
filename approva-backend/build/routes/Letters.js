"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _express = require("express");

var _express2 = _interopRequireDefault(_express);

var _sequelize = require("sequelize");

var _sequelize2 = _interopRequireDefault(_sequelize);

var _moment = require("moment");

var _moment2 = _interopRequireDefault(_moment);

var _models = require("../models");

var _constants = require("../constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const LetterRouter = _express2.default.Router();

LetterRouter.get("/", async (req, res) => {
  let filterType = req.query.type;
  let from = req.query.from ? (0, _moment2.default)(req.query.from) : (0, _moment2.default)().add("day", -7);
  let to = req.query.to ? (0, _moment2.default)(req.query.to) : (0, _moment2.default)();

  let where = {};
  if (filterType) where.direction = filterType;
  try {
    let data = await _models.Letter.findAll({
      include: [{
        model: _models.Disposition,
        include: _models.DispositionTarget
      }],
      where: _sequelize2.default.and(where, _sequelize2.default.where(_sequelize2.default.fn("date", _sequelize2.default.col("letter_date")), ">=", from.format("YYYY-MM-DD")), _sequelize2.default.where(_sequelize2.default.fn("date", _sequelize2.default.col("letter_date")), "<=", to.format("YYYY-MM-DD")))
    });

    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

LetterRouter.post("/", async (req, res) => {
  try {
    let checkNo = await _models.Letter.findOne({
      where: {
        letter_no: req.body.letter_no
      }
    });
    if (checkNo) {
      res.send(422).end();
      return;
    }
    let count = await _models.Letter.count({
      where: {
        periode: (0, _moment2.default)().format("YYYYMM")
      }
    });
    let create = await _models.Letter.create(_extends({}, req.body, {
      periode: (0, _moment2.default)().format("YYYYMM"),
      registry_no: count + 1
    }));

    res.json(create, 201);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

LetterRouter.post("/:id/scan", _constants.DocumentStorage.single("scanimg"), async (req, res) => {
  try {
    let file = req.file;
    if (!file) {
      res.status(400).end();
      return;
    }

    let check = await _models.Letter.findByPrimary(req.params.id);
    if (!check) {
      res.status(422).end();
      return;
    }
    console.log(file);
    res.json((await _models.Letter.update({
      scan: file.filename
    }, {
      where: {
        id: check.id
      }
    })));
  } catch (error) {
    res.status(error).end();
    console.log(error);
  }
});
LetterRouter.get("/registry", async (req, res) => {
  try {
    let count = await _models.Letter.count({
      where: {
        periode: (0, _moment2.default)().format("YYYYMM")
      }
    });

    res.json({
      reg_no: count
    });
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});
LetterRouter.put("/:id", async (req, res) => {
  try {
    let data = await _models.Letter.update(req.body, { where: { id: req.params.id } });
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

LetterRouter.delete("/:id", async (req, res) => {
  try {
    let del = await _models.Letter.destroy({ where: { id: req.params.id } });
    res.json(del);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

LetterRouter.get("/:id/dispositions", async (req, res) => {
  try {
    let data = await _models.Disposition.findAll({
      include: [_models.DispositionTarget],
      where: {
        letter_id: req.params.id
      }
    });
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

exports.default = LetterRouter;