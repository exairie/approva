"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _express = require("express");

var _express2 = _interopRequireDefault(_express);

var _sequelize = require("sequelize");

var _sequelize2 = _interopRequireDefault(_sequelize);

var _moment = require("moment");

var _moment2 = _interopRequireDefault(_moment);

var _models = require("../models");

var _constants = require("../constants");

var _jwt = require("../libs/jwt");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const AdminRouter = _express2.default.Router();

AdminRouter.get("/", async (req, res) => {
  try {
    let data = await _models.Admin.findAll({
      include: []
    });

    res.json(data);
  } catch (error) {}
});

AdminRouter.post("/", async (req, res) => {
  try {
    let checkNo = await _models.Admin.findOne({
      where: {
        username: req.body.username
      }
    });
    if (checkNo) {
      res.send(422).end();
      return;
    }

    let create = await _models.Letter.create(req.body);

    res.json(create, 201);
  } catch (error) {}
});

AdminRouter.put("/:id", async (req, res) => {
  try {
    let data = await _models.Admin.update(req.body, { where: { id: req.params.id } });
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

AdminRouter.delete("/:id", async (req, res) => {
  try {
    let del = await _models.Admin.destroy({ where: { id: req.params.id } });
    res.json(del);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});
AdminRouter.post("/login", async (req, res) => {
  try {
    let data = await _models.Admin.findOne({
      where: {
        username: req.body.username,
        password: req.body.password
      }
    });
    if (data) {
      var token = (0, _jwt.createJWToken)(data);
      data = JSON.parse(JSON.stringify(data));
      res.json(_extends({}, data, { token: token }));
    } else {
      res.status(400).json(data);
    }
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});
AdminRouter.post("/checktoken", async (req, res) => {
  try {
    let tokenData = await (0, _jwt.verify)(req.body.token);
    if (tokenData) res.json(tokenData);
  } catch (error) {
    console.log(error);
    res.status(400).json(error);
  }
});
AdminRouter.get("/:id/dispositions", async (req, res) => {
  try {
    let data = await _models.Disposition.findAll({
      include: [_models.DispositionTarget],
      where: {
        letter_id: req.params.id
      }
    });
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

exports.default = AdminRouter;