"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require("express");

var _express2 = _interopRequireDefault(_express);

var _sequelize = require("sequelize");

var _sequelize2 = _interopRequireDefault(_sequelize);

var _moment = require("moment");

var _moment2 = _interopRequireDefault(_moment);

var _models = require("../models");

var _constants = require("../constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const DispositionRouter = _express2.default.Router();

DispositionRouter.get("/", async (req, res) => {
  let filterType = req.query.type;
  let from = req.query.from ? (0, _moment2.default)(req.query.from) : (0, _moment2.default)().add("day", -7);
  let to = req.query.to ? (0, _moment2.default)(req.query.to) : (0, _moment2.default)();

  let where = {};
  if (filterType) where.direction = filterType;
  try {
    let data = await _models.Disposition.findAll({
      include: [{
        model: _models.Letter
      }, { model: DispositionRouter }],
      where: _sequelize2.default.and(where, _sequelize2.default.where(_sequelize2.default.fn("date", _sequelize2.default.col("created_at")), ">=", from.format("YYYY-MM-DD")), _sequelize2.default.where(_sequelize2.default.fn("date", _sequelize2.default.col("created_at")), "<=", to.format("YYYY-MM-DD")))
    });

    res.json(data);
  } catch (error) {}
});

DispositionRouter.post("/", async (req, res) => {
  try {
    let checkNo = await _models.Disposition.findOne({
      where: {
        letter_id: req.body.letter_id
      }
    });
    if (checkNo) {
      res.send(422).end();
      return;
    }

    let create = await _models.Disposition.create(req.body, {
      include: [{ model: _models.DispositionTarget }]
    });

    res.status(201).json(create);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

DispositionRouter.post("/:id/dispositionresult", _constants.DispositionStorage.single("scanimg"), async (req, res) => {
  try {
    let file = req.file;
    if (!file) {
      res.status(400).end();
      return;
    }

    let check = await _models.Disposition.findByPrimary(req.params.id);
    if (!check) {
      res.status(422).end();
      return;
    }

    res.json((await _models.Disposition.update({
      scan: file.filename
    }, {
      where: {
        id: check.id
      }
    })));
  } catch (error) {
    res.status(error).end();
    console.log(error);
  }
});

DispositionRouter.put("/:id", async (req, res) => {
  try {
    let data = await _models.Disposition.update(req.body, {
      where: { id: req.params.id }
    });
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

DispositionRouter.delete("/:id", async (req, res) => {
  try {
    let del = await _models.Disposition.destroy({ where: { id: req.params.id } });
    res.json(del);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

DispositionRouter.get("/:id/targets", async (req, res) => {
  try {
    let data = await _models.DispositionTarget.findAll({
      where: {
        disposition_id: req.params.id
      }
    });
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

exports.default = DispositionRouter;