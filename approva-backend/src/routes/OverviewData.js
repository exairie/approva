import Express from "express";
import Sequelize from "sequelize";
import moment from "moment";

import db from "../db";

const OverviewRouter = Express.Router();

OverviewRouter.get("/:month?", async (req, res) => {
  try {
    let from = moment();
    let to = moment();

    from.set("date", 1);
    to.add("month", 1);
    to.set("date", 1);
    to.add("day", -1);

    let thisMonth = await db.query(
      `SELECT 
      sum(case when direction = 'in' then 1 else 0 end) as incoming,
      sum(case when direction = 'out' then 1 else 0 end) as outgoing 
      FROM letters 
      where letter_date between '${from.format("YYYY-MM-DD")}' and '${to.format(
        "YYYY-MM-DD"
      )}'`,
      { type: Sequelize.QueryTypes.SELECT }
    );
    let letterChart = await db.query(
      `
      SELECT 
      sum(case when direction = 'in' then 1 else 0 end) as incoming,
      sum(case when direction = 'out' then 1 else 0 end) as outgoing,
      date(letter_date) as date
      from letters group by date(letter_date)`,
      { type: Sequelize.QueryTypes.SELECT }
    );
    let senderChart = await db.query(
      `
      SELECT
      count(*) as letter_count, 
      sender
      from letters group by sender`,
      { type: Sequelize.QueryTypes.SELECT }
    );
    let disposition = await db.query(
      `SELECT 
      count(*) as letter_count,
      sum(case when acknowledge is null then 0 else 1 end) as finished 
      from dispositions
      where created_at between '${from.format("YYYY-MM-DD")}' 
      and '${to.format("YYYY-MM-DD")}'`,
      { type: Sequelize.QueryTypes.SELECT }
    );
    res.json({
      thisMonth: thisMonth[0],
      letterChart: letterChart,
      senderChart: senderChart,
      dispositionCount: disposition[0]
    });
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

export default OverviewRouter;
