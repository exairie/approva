import Express from "express";
import Sequelize from "sequelize";
import moment from "moment";
import { Letter, Disposition, DispositionTarget } from "../models";
import { DispositionStorage } from "../constants";

const DispositionRouter = Express.Router();

DispositionRouter.get("/", async (req, res) => {
  let filterType = req.query.type;
  let from = req.query.from ? moment(req.query.from) : moment().add("day", -7);
  let to = req.query.to ? moment(req.query.to) : moment();

  let where = {};
  if (filterType) where.direction = filterType;
  try {
    let data = await Disposition.findAll({
      include: [
        {
          model: Letter
        },
        { model: DispositionRouter }
      ],
      where: Sequelize.and(
        where,
        Sequelize.where(
          Sequelize.fn("date", Sequelize.col("created_at")),
          ">=",
          from.format("YYYY-MM-DD")
        ),
        Sequelize.where(
          Sequelize.fn("date", Sequelize.col("created_at")),
          "<=",
          to.format("YYYY-MM-DD")
        )
      )
    });

    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

DispositionRouter.post("/", async (req, res) => {
  try {
    let checkNo = await Disposition.findOne({
      where: {
        letter_id: req.body.letter_id
      }
    });
    if (checkNo) {
      res.send(422).end();
      return;
    }

    let create = await Disposition.create(req.body, {
      include: [{ model: DispositionTarget }]
    });

    res.status(201).json(create);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

DispositionRouter.post(
  "/:id/dispositionresult",
  DispositionStorage.single("scanimg"),
  async (req, res) => {
    try {
      let file = req.file;
      if (!file) {
        res.status(400).end();
        return;
      }

      let check = await Disposition.findByPk(req.params.id);
      if (!check) {
        res.status(422).end();
        return;
      }

      res.json(
        await Disposition.update(
          {
            scan: file.filename,
            acknowledge: moment()
          },
          {
            where: {
              id: check.id
            }
          }
        )
      );
    } catch (error) {
      res.status(error).end();
      console.log(error);
    }
  }
);

DispositionRouter.put("/:id", async (req, res) => {
  try {
    let data = await Disposition.update(req.body, {
      where: { id: req.params.id }
    });
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

DispositionRouter.delete("/:id", async (req, res) => {
  try {
    let del = await Disposition.destroy({ where: { id: req.params.id } });
    res.json(del);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

DispositionRouter.get("/:id/targets", async (req, res) => {
  try {
    let data = await DispositionTarget.findAll({
      where: {
        disposition_id: req.params.id
      }
    });
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

export default DispositionRouter;
