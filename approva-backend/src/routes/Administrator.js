import Express from "express";
import Sequelize from "sequelize";
import moment from "moment";
import { Letter, Disposition, DispositionTarget, Admin } from "../models";
import { DocumentStorage } from "../constants";
import { createJWToken, verify } from "../libs/jwt";

const AdminRouter = Express.Router();

AdminRouter.get("/", async (req, res) => {
  try {
    let data = await Admin.findAll({
      include: []
    });

    res.json(data);
  } catch (error) {}
});

AdminRouter.post("/", async (req, res) => {
  try {
    let checkNo = await Admin.findOne({
      where: {
        username: req.body.username
      }
    });
    if (checkNo) {
      res.send(422).end();
      return;
    }

    let create = await Letter.create(req.body);

    res.json(create, 201);
  } catch (error) {}
});

AdminRouter.put("/:id", async (req, res) => {
  try {
    let data = await Admin.update(req.body, { where: { id: req.params.id } });
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

AdminRouter.delete("/:id", async (req, res) => {
  try {
    let del = await Admin.destroy({ where: { id: req.params.id } });
    res.json(del);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});
AdminRouter.post("/login", async (req, res) => {
  try {
    let data = await Admin.findOne({
      where: {
        username: req.body.username,
        password: req.body.password
      }
    });
    if (data) {
      var token = createJWToken(data);
      data = JSON.parse(JSON.stringify(data));
      res.json({ ...data, token: token });
    } else {
      res.status(400).json(data);
    }
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});
AdminRouter.post("/checktoken", async (req, res) => {
  try {
    let tokenData = await verify(req.body.token);
    if (tokenData) res.json(tokenData);
  } catch (error) {
    console.log(error);
    res.status(400).json(error);
  }
});
AdminRouter.get("/:id/dispositions", async (req, res) => {
  try {
    let data = await Disposition.findAll({
      include: [DispositionTarget],
      where: {
        letter_id: req.params.id
      }
    });
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

export default AdminRouter;
