import Express from "express";
import Sequelize from "sequelize";
import moment from "moment";
import {
  Letter,
  Disposition,
  DispositionTarget,
  Admin,
  DispositionTargetMaster
} from "../models";
import { DocumentStorage } from "../constants";

const DispTargetRouter = Express.Router();

DispTargetRouter.get("/", async (req, res) => {
  console.log("BA");
  try {
    res.json(await DispositionTargetMaster.findAll());
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

DispTargetRouter.post("/", async (req, res) => {
  try {
    let checkNo = await DispositionTargetMaster.findOne({
      where: {
        id_number: req.body.id_number,
        id_type: req.body.id_type
      }
    });
    if (checkNo) {
      res.send(422).end();
      return;
    }
    let create = await DispositionTargetMaster.create(req.body);
    res.json(create, 201);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});
DispTargetRouter.put("/:id", async (req, res) => {
  try {
    let d = await DispositionTargetMaster.findByPrimary(req.params.id);
    if (!d) {
      res.status(400).end();
      return;
    }

    let upd = await DispositionTargetMaster.update(req.body, {
      where: { id: d.id }
    });
    res.json(upd);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

DispTargetRouter.delete("/:id", async (req, res) => {
  try {
    let del = await DispositionTargetMaster.destroy({
      where: { id: req.params.id }
    });
    res.json(del);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

export default DispTargetRouter;
