import Express from "express";
import Sequelize from "sequelize";
import moment from "moment";
import {
  Letter,
  Disposition,
  DispositionTarget,
  Admin,
  DispositionAdvancementMaster
} from "../models";
import { DocumentStorage } from "../constants";

const DispAdvRouter = Express.Router();

DispAdvRouter.get("/", async (req, res) => {
  try {
    res.json(await DispositionAdvancementMaster.findAll());
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

DispAdvRouter.post("/", async (req, res) => {
  try {
    let checkNo = await DispositionAdvancementMaster.findOne({
      where: {
        advancement: req.body.advancement
      }
    });
    if (checkNo) {
      res.send(422).end();
      return;
    }
    let create = await DispositionAdvancementMaster.create({
      ...req.body,
      options: JSON.stringify(req.body.options)
    });
    res.json(create, 201);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});
DispAdvRouter.put("/:id", async (req, res) => {
  try {
    let d = await DispositionAdvancementMaster.findByPrimary(req.params.id);
    if (!d) {
      res.status(400).end();
      return;
    }

    let upd = await DispositionAdvancementMaster.update(
      {
        ...req.body,
        options: JSON.stringify(req.body.options)
      },
      {
        where: { id: d.id }
      }
    );
    res.json(upd);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

DispAdvRouter.delete("/:id", async (req, res) => {
  try {
    let del = await DispositionAdvancementMaster.destroy({
      where: { id: req.params.id }
    });
    res.json(del);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

export default DispAdvRouter;
