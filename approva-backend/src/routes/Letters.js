import Express from "express";
import Sequelize from "sequelize";
import moment from "moment";
import { Letter, Disposition, DispositionTarget, Admin } from "../models";
import { DocumentStorage } from "../constants";

const LetterRouter = Express.Router();

LetterRouter.get("/", async (req, res) => {
  let filterType = req.query.type;
  let from = req.query.from ? moment(req.query.from) : moment().add("day", -7);
  let to = req.query.to ? moment(req.query.to) : moment();

  let where = {};
  if (filterType) where.direction = filterType;
  try {
    let data = await Letter.findAll({
      include: [
        {
          model: Disposition,
          include: DispositionTarget
        }
      ],
      where: Sequelize.and(
        where,
        Sequelize.where(
          Sequelize.fn("date", Sequelize.col("letter_date")),
          ">=",
          from.format("YYYY-MM-DD")
        ),
        Sequelize.where(
          Sequelize.fn("date", Sequelize.col("letter_date")),
          "<=",
          to.format("YYYY-MM-DD")
        )
      )
    });

    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

LetterRouter.post("/", async (req, res) => {
  try {
    let checkNo = await Letter.findOne({
      where: {
        letter_no: req.body.letter_no
      }
    });
    if (checkNo) {
      res.send(422).end();
      return;
    }
    let count = await Letter.count({
      where: {
        periode: moment().format("YYYYMM")
      }
    });
    let create = await Letter.create({
      ...req.body,
      periode: moment().format("YYYYMM"),
      registry_no: count + 1
    });

    res.json(create, 201);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});
LetterRouter.get("/:id/details", async (req, res) => {
  try {
    let data = await Letter.findByPk(req.params.id, {
      include: [
        { model: Disposition, include: [DispositionTarget] },
        { model: Admin }
      ]
    });
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});
LetterRouter.get("/:id/thumbnail", async (req, res) => {
  try {
    let letter = await Letter.findByPrimary(req.params.id);
    if (!letter) {
      res.status(404).end();
      return;
    }
    let img = letter.scan;
    let imgUri = `./uploads/documents/${img}`;
    let file = require("fs").readFileSync(imgUri);
    let stream = await pdf(file);
    console.log(stream);
    res.send(stream).end();
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});
LetterRouter.post(
  "/:id/scan",
  DocumentStorage.single("scanimg"),
  async (req, res) => {
    try {
      let file = req.file;
      if (!file) {
        res.status(400).end();
        return;
      }

      let check = await Letter.findByPrimary(req.params.id);
      if (!check) {
        res.status(422).end();
        return;
      }
      console.log(file);
      res.json(
        await Letter.update(
          {
            scan: file.filename
          },
          {
            where: {
              id: check.id
            }
          }
        )
      );
    } catch (error) {
      res.status(error).end();
      console.log(error);
    }
  }
);
LetterRouter.get("/registry", async (req, res) => {
  try {
    let count = await Letter.count({
      where: {
        periode: moment().format("YYYYMM")
      }
    });

    res.json({
      reg_no: Number(count) + 1
    });
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});
LetterRouter.put("/:id", async (req, res) => {
  try {
    let data = await Letter.update(req.body, { where: { id: req.params.id } });
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

LetterRouter.delete("/:id", async (req, res) => {
  try {
    let del = await Letter.destroy({ where: { id: req.params.id } });
    res.json(del);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

LetterRouter.get("/:id/dispositions", async (req, res) => {
  try {
    let data = await Disposition.findAll({
      include: [DispositionTarget],
      where: {
        letter_id: req.params.id
      }
    });
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});
LetterRouter.get("/archive", async (req, res) => {
  try {
    let direction = req.query.direction;
    let start = req.query.start || moment().add(-7, "days");
    let end = req.query.end || moment();
    let search = req.query.search || "";
    let origin = req.query.origin;
    let where = {};
    if (direction !== "all") {
      where.direction = direction;
    }
    if (origin) {
      where.origin = origin;
    }
    let letters = await Letter.findAll({
      where: Sequelize.and(
        where,
        {
          [Sequelize.Op.or]: {
            letter_no: {
              [Sequelize.Op.like]: `%${search}%`
            },
            title: {
              [Sequelize.Op.like]: `%${search}%`
            },
            sender: {
              [Sequelize.Op.like]: `%${search}%`
            },
            brief_content: {
              [Sequelize.Op.like]: `%${search}%`
            }
          }
        },
        Sequelize.where(
          Sequelize.fn("date", Sequelize.col("letter_date")),
          ">=",
          start.format("YYYY-MM-DD")
        ),
        Sequelize.where(
          Sequelize.fn("date", Sequelize.col("letter_date")),
          "<=",
          end.format("YYYY-MM-DD")
        )
      ),
      include: [{ model: Disposition, include: DispositionTarget }]
    });
    res.json(letters);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});
export default LetterRouter;
