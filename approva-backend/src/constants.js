import multer from "multer";
import moment from "moment";

const DocumentUploadStorage = multer.diskStorage({
  destination: "./uploads/documents",
  filename: (req, file, callback) => {
    callback(
      null,
      moment()
        .unix()
        .toString() + ".pdf"
    );
  }
});
const DispositionUploadStorage = multer.diskStorage({
  destination: "./uploads/dispositions",
  filename: (req, file, callback) => {
    callback(
      null,
      moment()
        .unix()
        .toString() + ".pdf"
    );
  }
});

export const DocumentStorage = multer({ storage: DocumentUploadStorage });
export const DispositionStorage = multer({ storage: DispositionUploadStorage });
