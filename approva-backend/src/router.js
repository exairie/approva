import LetterRouter from "./routes/Letters";
import DispositionRouter from "./routes/Disposition";
import AdminRouter from "./routes/Administrator";
import DispTargetRouter from "./routes/DispTargetMaster";
import DispAdvRouter from "./routes/DispAdvancement";
import OverviewRouter from "./routes/OverviewData";

export default app => {
  app.use("/letters", LetterRouter);
  app.use("/dispositions", DispositionRouter);
  app.use("/administrator", AdminRouter);
  app.use("/disposition_targets", DispTargetRouter);
  app.use("/disposition_advancements", DispAdvRouter);
  app.use("/overviewdata", OverviewRouter);
};
