import Admin from "./admins";
import DispositionTarget from "./disposition_targets";
import Disposition from "./dispositions";
import Letter from "./letters";
import DispositionAdvancementMaster from "./disposition_advancement_master";
import DispositionTargetMaster from "./disposition_target_master";

Disposition.hasMany(DispositionTarget);
DispositionTarget.belongsTo(Disposition);

Letter.hasMany(Letter, {
  as: "advancement",
  foreignKey: "origin",
  sourceKey: "id"
});
Letter.belongsTo(Letter, {
  as: "letter_origin",
  foreignKey: "origin",
  targetKey: "id"
});
Letter.hasOne(Disposition);
Letter.belongsTo(Admin);
Admin.hasMany(Letter);
export {
  Admin,
  DispositionTarget,
  Disposition,
  Letter,
  DispositionAdvancementMaster,
  DispositionTargetMaster
};
