import db from "../db";
import DataTypes from "sequelize";
export default db.define(
  "disposition_advancement_master",
  {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    advancement: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    type: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    options: {
      type: DataTypes.STRING(250),
      allowNull: false
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    }
  },
  {
    tableName: "disposition_advancement_master"
  }
);
