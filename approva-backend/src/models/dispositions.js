/* jshint indent: 2 */
import db from "../db";
import DataTypes from "sequelize";
export default db.define(
  "dispositions",
  {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    letter_id: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    disposition_date: {
      type: DataTypes.DATE,
      allowNull: false
    },
    acknowledge: {
      type: DataTypes.DATE,
      allowNull: true
    },
    due_date: {
      type: DataTypes.DATE,
      allowNull: false
    },
    importance_level: {
      type: DataTypes.INTEGER(10),
      allowNull: true
    },
    advancement: {
      type: DataTypes.STRING(250),
      allowNull: true
    },
    message: {
      type: DataTypes.STRING(250),
      allowNull: true
    },
    scan: {
      type: DataTypes.STRING(250),
      allowNull: true
    },

    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    }
  },
  {
    tableName: "dispositions"
  }
);
