/* jshint indent: 2 */
import db from "../db";
import DataTypes from "sequelize";
export default db.define(
  "letters",
  {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    periode: {
      type: DataTypes.CHAR(6),
      allowNull: false
    },
    letter_date: {
      type: DataTypes.DATE,
      allowNull: false
    },
    title: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    letter_no: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    registry_no: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    sender: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    brief_content: {
      type: DataTypes.STRING(250),
      allowNull: false
    },
    scan: {
      type: DataTypes.STRING(250),
      allowNull: false
    },
    status: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    direction: {
      type: DataTypes.STRING(5),
      allowNull: false
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    origin: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    admin_id: { type: DataTypes.INTEGER, allowNull: false }
  },
  {
    tableName: "letters"
  }
);
