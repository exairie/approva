import db from "../db";
import DataTypes from "sequelize";
export default db.define(
  "disposition_target_master",
  {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    id_type: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    id_number: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    active: {
      type: DataTypes.INTEGER(2),
      allowNull: false
    },
    role: {
      type: DataTypes.STRING(75),
      allowNull: false
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    }
  },
  {
    tableName: "disposition_target_master"
  }
);
