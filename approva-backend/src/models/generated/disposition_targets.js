/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('disposition_targets', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    disposition_id: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    disposition_order: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    target_name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    target_id: {
      type: DataTypes.STRING(250),
      allowNull: false
    },
    status: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    message: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    command: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    aswad_enrollment_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    tableName: 'disposition_targets'
  });
};
