import Express from "express";
import Cors from "cors";
import BodyParser from "body-parser";
import Router from "./router";
// import Http from "http";
import { staticFilepath } from "./env";

var server = Express();
// var http = Http.Server(server);

server.use((req, res, next) => {
  console.log(`Incoming request ${req.method} : ${req.path}`);
  next();
});

server.use(Cors());
server.use(BodyParser.urlencoded());
server.use(BodyParser.json());

server.use("/docscan", Express.static("./uploads/documents"));
server.use("/dispscan", Express.static("./uploads/dispositions"));
Router(server);
server.use("/", Express.static(staticFilepath + "/"));
server.all("*", async (req, res) => {
  try {
    res.sendFile(staticFilepath + "/index.html");
  } catch (error) {
    console.log(error);
    res.status(404).send("Not found");
  }
});

server.get("/ba", (req, res) => {
  console.log("SENDING ");
  res.send("Hello World").end();
});

server.listen(process.env.PORT || 4480, () => {
  console.log("Server started");
});
