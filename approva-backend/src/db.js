import sequelize from "sequelize";
import * as cfg from "./configs";
import moment from "moment-timezone";
import pexec from "child_process";

const db = new sequelize(cfg.database, cfg.username, cfg.password, {
  dialect: "mysql",
  username: cfg.username,
  password: cfg.password,
  host: cfg.host,
  dialectOptions: {
    // useUTC:false
    typeCast: function(field, next) {
      // for reading from database
      if (field.type === "DATETIME") {
        var time = moment.tz(field.string(), "Asia/Jakarta");
        // console.log(time.toString());
        // console.log(time);
        return time;
      }
      return next();
    }
  },
  define: {
    underscored: true
  },
  timezone: "+07:00" //for writing to database,
});

let authenticate = () => {
  db.authenticate()
    .then(() => {
      console.log("Database connected");
    })
    .catch(e => {
      console.log("Database connect failed. Retrying in 2 secs");
      console.log(e);
      setTimeout(() => {
        authenticate();
      }, 2000);
    });
};

authenticate();

export default db;
