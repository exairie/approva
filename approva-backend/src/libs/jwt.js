import jwt from "jsonwebtoken";
import { privatekey } from "../configs";
import _ from "lodash";

export async function middleware(req, res, next) {
  if (req.headers.token) {
    let token = req.headers.token;
    try {
      let tokenData = await verify(token);
      req.userdata = tokenData.data;

      next();
    } catch (e) {
      res.status(400).json(e);
    }
  } else {
    console.log("FORBIDDEN!");
    res.status(403).end();
  }
}
export function verify(token) {
  return new Promise((res, rej) => {
    jwt.verify(token, privatekey, (err, decodetoken) => {
      if (err || !decodetoken) {
        rej(err);
      }

      res(decodetoken);
    });
  });
}

export function createJWToken(details) {
  if (!details) return "";
  console.log("Creating token");
  console.log(typeof details);

  if (typeof details !== "object") {
    details = {};
    return "";
  }

  console.log(details);

  if (!details.maxAge || typeof details.maxAge !== "number") {
    details.maxAge = 999999999;
  }

  console.log(details);

  details.sessionData = _.reduce(
    details.sessionData || {},
    (memo, val, key) => {
      if (typeof val !== "function" && key !== "password") {
        memo[key] = val;
      }
      return memo;
    },
    {}
  );

  console.log(details);

  let token = jwt.sign(
    {
      data: details
    },
    privatekey,
    {
      expiresIn: details.maxAge,
      algorithm: "HS256"
    }
  );

  return token;
}
