import Vue from "vue";
import VueRouter from "vue-router";
import App from "./App.vue";

// LightBootstrap plugin
import LightBootstrap from "./light-bootstrap-main";
import { Plugin as Fragment } from "vue-fragment";
// router setup
import routes from "./routes/routes";
import Vuex from "vuex";
import store from "./stores";
import "./registerServiceWorker";
import VueFlashMessage from "vue-flash-message";
import Datatable from "vue2-datatable-component";

// plugin setup

import "vue-flash-message/dist/vue-flash-message.min.css";

Vue.use(VueFlashMessage, {
  messageOptions: {
    timeout: 3000
  }
});
Vue.use(Datatable); // done!
Vue.use(Fragment);
Vue.use(VueRouter);
Vue.use(LightBootstrap);

// configure router
const router = new VueRouter({
  routes, // short for routes: routes
  linkActiveClass: "nav-item active",
  scrollBehavior: to => {
    if (to.hash) {
      return { selector: to.hash };
    } else {
      return { x: 0, y: 0 };
    }
  }
});

/* eslint-disable no-new */
new Vue({
  store,
  el: "#app",
  render: h => h(App),
  router
});
