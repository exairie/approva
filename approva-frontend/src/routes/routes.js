import DashboardLayout from "../layout/DashboardLayout.vue";
// GeneralViews
import NotFound from "../pages/NotFoundPage.vue";

// Admin pages
import Overview from "src/pages/Overview.vue";
import UserProfile from "src/pages/UserProfile.vue";
import TableList from "src/pages/TableList.vue";
import Typography from "src/pages/Typography.vue";
import Icons from "src/pages/Icons.vue";
import Maps from "src/pages/Maps.vue";
import Notifications from "src/pages/Notifications.vue";
import Upgrade from "src/pages/Upgrade.vue";

/** User page */
import Login from "../pages/Login.vue";

import AdministratorPage from "../pages/Administrators";

import Inbox from "../pages/Inbox";
import Outbox from "../pages/Outbox";
import Disposisi from "../pages/Disposisi";
import LembarDisposisi from "../pages/DispositionSheet";
import DispositionTargets from "../pages/DispositionTargets";
import DispositionAdvancements from "../pages/DispositionAdvancements";
import Archive from "../pages/Archive";
const routes = [
  {
    path: "/",
    component: DashboardLayout,
    redirect: "/admin/overview"
  },
  {
    path: "/lembardisposisi",
    name: "Lembar Disposisi",
    component: LembarDisposisi
  },
  {
    path: "/login",
    component: Login
  },
  {
    path: "/admin",
    component: DashboardLayout,
    redirect: "/admin/inbox",
    children: [
      {
        path: "inbox",
        name: "Surat Masuk",
        component: Inbox
      },
      {
        path: "outbox",
        name: "Surat Keluar",
        component: Outbox
      },
      {
        path: "disposisi",
        name: "Disposisi Surat",
        component: Disposisi
      },
      {
        path: "targetdisposisi",
        name: "Target Disposisi",
        component: DispositionTargets
      },
      {
        path: "perintahdisposisi",
        name: "Perintah Disposisi",
        component: DispositionAdvancements
      },
      {
        path: "arsip",
        name: "Arsip Surat Masuk/Keluar",
        component: Archive
      },
      {
        path: "overview",
        name: "Overview",
        // redirect: "/admin/inbox",
        component: Overview
      },
      {
        path: "administrators",
        name: "Administrator List",
        component: AdministratorPage
      },
      {
        path: "user",
        name: "User",
        component: UserProfile
      },
      {
        path: "table-list",
        name: "Table List",
        component: TableList
      },
      {
        path: "typography",
        name: "Typography",
        component: Typography
      },
      {
        path: "icons",
        name: "Icons",
        component: Icons
      },
      {
        path: "maps",
        name: "Maps",
        component: Maps
      },
      {
        path: "notifications",
        name: "Notifications",
        component: Notifications
      },
      {
        path: "upgrade",
        name: "Upgrade to PRO",
        component: Upgrade
      }
    ]
  },
  { path: "*", component: NotFound }
];

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes;
