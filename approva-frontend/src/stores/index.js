import Vuex from "vuex";
import UserInfo from "./UserInfo";
import Administrators from "./Administrators";
import TransactionTypes from "./TransactionTypes";
import Inbox from "./Inbox";
import Outbox from "./Outbox";
import Disposition from "./Disposition";
import DispositionTargets from "./DispositionTargets";
import Aswad from "./Aswad";
import DispositionTargetMaster from "./DispositionTargetMaster";
import DispositionAdvancements from "./DispositionAdvancements";
import Archive from "./Archive";
import Vue from "vue";
Vue.use(Vuex);
export default new Vuex.Store({
  modules: {
    UserInfo,
    Administrators,
    TransactionTypes,
    Inbox,
    Outbox,
    Disposition,
    DispositionTargets,
    Aswad,
    DispositionTargetMaster,
    DispositionAdvancements,
    Archive
  }
});
