import moment from "moment";
import Axios from "axios";
import { aswadServer } from "../configs";

export default {
  namespaced: true,
  state: {
    enrollments: []
  },
  mutations: {
    setData(state, data) {
      switch (data.type) {
        case "enrollment":
          {
            state.enrollments = data.data;
          }
          break;
      }
    }
  },
  actions: {
    async loadEnrollments(context) {
      try {
        let resp = await Axios.get(`${aswadServer}/attendance_log/log`);
        if (resp.status == 200) {
          context.commit("setData", { type: "enrollment", data: resp.data });
        }
      } catch (error) {}
    }
  }
};
