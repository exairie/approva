import moment from "moment";
import Axios from "axios";
import { serverUrl } from "../configs";

export default {
  namespaced: true,
  state: {
    data: [],
    loading: false,
    error: ""
  },
  mutations: {
    setData(state, data) {
      state.data = data;
    },
    setLoading(state, isLoading) {
      state.loading = isLoading;
    },
    setError(state, error) {
      state.error = error;
    }
  },
  actions: {
    async load(context, payload) {
      try {
        context.commit("setLoading", true);
        let resp = await Axios.get(`${serverUrl}/letters/archive/`, {
          params: {
            search: payload.search,
            from: payload.from,
            to: payload.to,
            direction: payload.direction || "all",
            origin: payload.origin
          }
        });
        if (resp.status == 200) {
          context.commit("setError", null);

          context.commit("setData", resp.data);
        }
      } catch (e) {
        console.log(e);
        context.commit("setError", e.message);
      } finally {
        context.commit("setLoading", false);
      }
    }
  }
};
