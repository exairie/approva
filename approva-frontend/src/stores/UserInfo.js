import moment from "moment";

export default {
  namespaced: true,
  state: {
    userinfo: JSON.parse(window.localStorage.getItem("accappuserinfo")),
    token: window.localStorage.getItem("accapptoken"),
    lastLogin: null
  },
  mutations: {
    setUserInfo(state, payload) {
      state.lastLogin = moment();
      state.userinfo = payload;
      state.token = payload.token;

      window.localStorage.setItem("accapptoken", state.token);
      window.localStorage.setItem(
        "accappuserinfo",
        JSON.stringify(state.userinfo)
      );
      console.log("Access Token Saved");
    }
  }
};
