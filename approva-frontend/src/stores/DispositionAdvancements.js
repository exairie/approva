import moment from "moment";
import Axios from "axios";
import { serverUrl } from "../configs";

export default {
  namespaced: true,
  state: {
    data: []
  },
  mutations: {
    setData(state, data) {
      state.data = data.map(x => {
        return {
          ...x,
          options:
            typeof x.options === "string" ? JSON.parse(x.options) : x.options
        };
      });
    }
  },
  actions: {
    async loadData(context) {
      try {
        let resp = await Axios.get(`${serverUrl}/disposition_advancements`);
        if (resp.status == 200) {
          context.commit("setData", resp.data);
        }
      } catch (error) {
        console.log(error);
      }
    }
  }
};
